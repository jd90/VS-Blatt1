package aqua.blatt1.broker;

import java.io.Serializable;
import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import messaging.Endpoint;
import messaging.Message;
import aqua.blatt1.common.Direction;
import aqua.blatt1.common.FishModel;
import aqua.blatt1.common.msgtypes.DeregisterRequest;
import aqua.blatt1.common.msgtypes.HandoffRequest;
import aqua.blatt1.common.msgtypes.NeighborUpdate;
import aqua.blatt1.common.msgtypes.RegisterRequest;
import aqua.blatt1.common.msgtypes.RegisterResponse;
import aqua.blatt1.common.msgtypes.Token;
import aqua.blatt2.broker.PoisonPill;

public class Broker {
	private Endpoint endpoint;
	private ClientCollection<InetSocketAddress> clients;
	private int id = 0;
	private int POOL_SIZE = 3;
	private ExecutorService executor = Executors.newFixedThreadPool(POOL_SIZE);
	private boolean done = false;
	private ReadWriteLock lock = new ReentrantReadWriteLock();
	
	
	
	public Broker() {
		endpoint = new Endpoint(4711);
		clients = new ClientCollection<InetSocketAddress>();
	}
	
	private class BrokerTask implements Runnable {
		Message m;
		private BrokerTask(Message m) {
			this.m = m;
		}
		@Override
		public void run() {
			Serializable s = m.getPayload();
			
			if (s instanceof RegisterRequest){
				register(m);
				
			} else if (s instanceof DeregisterRequest) {
				deregister(m);
			} else if (s instanceof HandoffRequest) {
				handoffFish(m);
			} else if (s instanceof PoisonPill) {
				done = true;
			}
			
		}
	}
	
	public void broker() {
		while(!done) {
			Message m = endpoint.blockingReceive();
			executor.execute(new BrokerTask(m));
		}
		executor.shutdown();
	}
	
	public void register(Message msg) {
		String idname = "Tank " + id;
		lock.writeLock().lock();
		clients.add(idname, msg.getSender());
		lock.writeLock().unlock();
		InetSocketAddress left = clients.getLeftNeighorOf(clients.indexOf(msg.getSender()));
		InetSocketAddress right = clients.getRightNeighorOf(clients.indexOf(msg.getSender()));
		
		
		//neuer Client rechts + links mitteilen
		endpoint.send(msg.getSender(), new NeighborUpdate(left, right));
		
		//linker client msg.getsender() als rechts mitteilen
		endpoint.send(left, new NeighborUpdate(null, msg.getSender()));
		
		//rechter client msg.getsender() als links mitteilen
		endpoint.send(right, new NeighborUpdate(msg.getSender(), null));
		
		endpoint.send(msg.getSender(), new RegisterResponse(idname));
		
		if (id == 0)
			endpoint.send(msg.getSender(), new Token());
		
		
		id++;
		
		System.out.println("Register: " + msg.getSender().toString());
	}
	
	public void deregister(Message msg) {
		
		
		InetSocketAddress left = clients.getLeftNeighorOf(clients.indexOf(msg.getSender()));
		InetSocketAddress right = clients.getRightNeighorOf(clients.indexOf(msg.getSender()));
		
		// right ist neuer right von left
		endpoint.send(left, new NeighborUpdate(null, right));
		
		// left ist neuer left von right
		endpoint.send(right, new NeighborUpdate(left, null));
		
		lock.writeLock().lock();
		clients.remove(clients.indexOf(msg.getSender()));
		lock.writeLock().unlock();
		
		
		System.out.println("Deregister: " + msg.getSender().toString());
	}
	
	public void handoffFish(Message msg) {
		HandoffRequest s = (HandoffRequest) msg.getPayload();
		FishModel f = s.getFish();
		Direction direction = f.getDirection();
		InetSocketAddress receiver = null;
		lock.readLock().lock();
		if (direction == Direction.LEFT) {
			receiver = clients.getLeftNeighorOf(clients.indexOf(msg.getSender()));
			
		} else if (direction == Direction.RIGHT){
			receiver = clients.getRightNeighorOf(clients.indexOf(msg.getSender()));
		}
		lock.readLock().unlock();
		endpoint.send(receiver, msg.getPayload());

	}
	
	public static void main(String[] args) {
		Broker b = new Broker();
		
		b.broker();
	}
	
}
